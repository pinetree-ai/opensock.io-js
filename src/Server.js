"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const WebSocket = __importStar(require("isomorphic-ws"));
const uuid_1 = require("uuid");
const events_1 = require("./events");
const axios_1 = __importDefault(require("axios"));
const cors_1 = __importDefault(require("cors"));
const http_1 = __importDefault(require("http"));
const events_2 = require("./utils/events");
const events_3 = __importDefault(require("events"));
const algorithm_1 = require("./utils/algorithm");
const stringify = require('json-stringify-safe');
class Server {
    constructor(primary_url, connection_token, scale_token, max_connections) {
        // handles the connection (once connection request has been authenticated and a server has been assigned)
        this.handleConnection = (ws, room_id) => {
            const connection_id = (0, uuid_1.v4)();
            // add the websocket object to the map of connections on the server 
            this.connections[connection_id] = ws;
            // if a room id has been specified 
            if (room_id) {
                // try to find a room with this room id 
                const target_room = this.rooms.find(room => room.id === room_id);
                // if the room already exists add the connection id to it 
                if (target_room) {
                    target_room.connections.push(connection_id);
                }
                // create the room if it doesn't exist
                else {
                    this.rooms.push({
                        id: room_id,
                        connections: [connection_id],
                        migrating: false,
                        ack_count: 0
                    });
                }
                // if this is the primary server update the state 
                if (this.isPrimary()) {
                    // find the index of the target node (primary node)
                    const target_node_index = this.primaryState.nodes.findIndex(node => node.url === this.getHostname());
                    this.primaryState.nodes[target_node_index].total_connections++;
                    if (target_room) {
                        const room_index = this.primaryState.nodes[target_node_index].rooms.findIndex(room => room.id === target_room.id);
                        this.primaryState.nodes[target_node_index].rooms[room_index].num_connections++;
                    }
                    else {
                        this.primaryState.nodes[target_node_index].rooms.push({
                            id: room_id,
                            num_connections: 1
                        });
                    }
                }
                // send an api request to primary to update state 
                else {
                    const api_endpoint = `${this.getPrimaryRestHost()}/nodes/update`;
                    (0, axios_1.default)(api_endpoint, {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        data: {
                            server_url: this.getHostname(),
                            rooms: this.rooms.map(room => ({ num_connections: room.connections.length, id: room.id })),
                            total_connections: Object.keys(this.connections).length
                        }
                    }).catch(err => console.log(err));
                }
                // call the on connection handler 
                this.onconnection(ws, this.state, target_room || { id: room_id, connections: [connection_id], migrating: false, ack_count: 0 });
            }
            else {
                // if this is the primary server, update the primary state directly
                if (this.isPrimary()) {
                    const target_node_index = this.primaryState.nodes.findIndex(node => node.url === this.getWsHostname());
                    this.primaryState.nodes[target_node_index].total_connections++;
                }
                // send an api request to primary to update the state 
                else {
                    const api_endpoint = `${this.getPrimaryRestHost()}/nodes/update`;
                    (0, axios_1.default)(api_endpoint, {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        data: {
                            server_url: this.getHostname(),
                            total_connections: Object.keys(this.connections).length
                        }
                    }).catch(err => console.log(err.response));
                }
                // call the on connection handler 
                this.onconnection(ws, this.state);
            }
            // publish a connection success event to the client 
            const payload = (0, events_2.createEventPayload)(events_1.Events.CONNECTION_SUCCESS_EVENT, {
                data: {
                    message: "Connection established successfully"
                }
            });
            // emit a internal secondary connection event (to keep track of whether all clients have reconnected after migration)
            this.eventEmitter.emit('SECONDARY_CONNECTION', Object.keys(this.connections).length);
            ws.send(payload);
        };
        /* ---------------------------------------- PRIMARY REST API HANDLERS ---------------------------------------- */
        // to get the state from the primary state 
        this.getState = (req, res) => {
            res.status(200).json({ primaryState: this.primaryState, state: this.state });
        };
        // list all roooms for a specific server
        this.listRooms = (req, res) => {
            const { server_url } = req.body;
            const target_node = this.primaryState.nodes.find(node => node.url === server_url);
            if (!target_node) {
                res.status(404).json({ error: "node not found" });
            }
            res.status(200).json({ rooms: target_node.rooms });
        };
        // registers a secondary server 
        this.registerSecondary = (req, res) => {
            const { url } = req.body;
            this.primaryState.nodes.push({
                url,
                rooms: [],
                total_connections: 0,
                lock: false
            });
            res.status(200).json({ registered: true });
        };
        // derigesters a secondary server 
        this.deregisterSecondary = (req, res) => {
            const { server_url } = req.params;
            const target_node_index = this.primaryState.nodes.findIndex(node => node.url === server_url);
            const target_node = this.primaryState.nodes[target_node_index];
            if (!target_node) {
                return res.status(400).send({ error: "node not found" });
            }
            // check if there are rooms present on the server 
            if (target_node.rooms.length > 0) {
                return res.status(406).send({ error: "Server not cleared" });
            }
            // delete the target node from the state 
            this.primaryState.nodes.splice(target_node_index, 1);
            res.status(200).json({ message: "ok" });
        };
        // receives state updates from secondary servers and updates the primary state 
        this.handleStateUpdates = (req, res) => {
            console.log("Update state request received: ", req.body);
            const { rooms, total_connections, server_url } = req.body;
            const target_node_index = this.primaryState.nodes.findIndex(node => node.url === server_url);
            const target_node = this.primaryState.nodes[target_node_index];
            if (!target_node) {
                return res.status(400).send({ error: "node not found" });
            }
            this.primaryState.nodes[target_node_index].rooms = rooms ? rooms : this.primaryState.nodes[target_node_index].rooms;
            this.primaryState.nodes[target_node_index].total_connections = total_connections;
            res.status(200).json({ message: "ok" });
        };
        // called to initiate migration of a room to a particular server 
        this.migrateRoom = (req, res) => __awaiter(this, void 0, void 0, function* () {
            console.log("Migration request received");
            // rooo id of room to migrate 
            const { room_id } = req.params;
            // url of the server to migrate to 
            const { to } = req.body;
            // find the node with this room id 
            const target_node_url = this.findNodeWithRoom(room_id);
            // the internal room takeover endpoint of the destination node 
            const destination_endpoint = `http://${to}/internal/rooms/${room_id}/takeover`;
            console.log("to: ", to);
            console.log("room id: ", room_id);
            console.log("destination: ", destination_endpoint);
            try {
                // sends API request to destination (srv2) indicating the migration
                const response = yield (0, axios_1.default)(destination_endpoint, {
                    method: "POST",
                    data: {
                        from: target_node_url
                    }
                });
                const { room } = response.data;
                // update the state of the primary node 
                if (room) {
                    const target_node_index = this.primaryState.nodes.findIndex(node => node.url === target_node_url);
                    this.primaryState.nodes[target_node_index].rooms.push(room);
                }
                res.status(200).json({ message: "ok" });
            }
            catch (err) {
                console.log(err.response.data);
                res.status(500).json({ error: true });
            }
        });
        this.vacateNode = (req, res) => __awaiter(this, void 0, void 0, function* () {
            console.log("Request to vacate node received");
            // the url of the server to vacate
            const { server_url } = req.params;
            console.log("server_url", server_url);
            // the node object of the server to vacate in the primary state 
            const vacate_node = this.primaryState.nodes.find(node => node.url === server_url);
            // all the rooms on the node to vacate 
            const vacate_node_rooms = vacate_node.rooms.map(room => room.id);
            // all the nodes except the node to vacate 
            const available_servers = this.primaryState.nodes.map(node => node.url).filter(node => node !== server_url && node !== this.getHostname());
            // distribute the rooms between the available servers 
            const server_room_map = (0, algorithm_1.distribute)(vacate_node_rooms, available_servers);
            console.log("server room map: ", server_room_map);
            // waits for all the migrations to complete when used with the await keyword 
            const migrate_rooms = () => {
                return new Promise((res, rej) => __awaiter(this, void 0, void 0, function* () {
                    let promises = [];
                    for (let server of available_servers) {
                        let rooms = server_room_map[server];
                        if (rooms.length === 0) {
                            break;
                        }
                        for (let room of rooms) {
                            console.log("server: ", server);
                            promises.push((0, axios_1.default)(`http://localhost:8002/rooms/${room}/migrate`, {
                                method: "POST",
                                data: {
                                    to: server
                                }
                            }).catch(err => console.log(err.response.data)));
                        }
                    }
                    try {
                        let results = yield Promise.all(promises);
                        res(results);
                    }
                    catch (err) {
                        rej(err);
                    }
                }));
            };
            // trigger migration flows for all the rooms 
            try {
                let results = yield migrate_rooms();
                console.log(results);
                res.status(200).send({ success: true });
            }
            catch (err) {
                res.status(500).send({ success: false });
            }
        });
        /* ---------------------------------------- PRIMARY REST API HANDLERS ---------------------------------------- */
        /* ---------------------------------------- SECONDARY REST API HANDLERS ---------------------------------------- */
        // called to takeover a room from a server 
        this.takeoverRoom = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { room_id } = req.params;
            const { from } = req.body;
            // Destination sends an internal migration API request to source
            const source_endpoint = `http://${from}/internal/rooms/${room_id}/migrate`;
            try {
                const response = yield (0, axios_1.default)(source_endpoint, {
                    method: "POST",
                    data: {
                        to: this.getHostname()
                    }
                });
                const { id, num_connections } = response.data;
                // create a room on this server 
                this.rooms.push({
                    id,
                    connections: [],
                    ack_count: 0,
                    migrating: false
                });
                // respond to the primary 
                res.status(200).json({
                    room: {
                        id: room_id,
                        num_connections
                    }
                });
                // wait for clients to join 
                let target_room = this.rooms[this.rooms.length - 1];
                console.log("Waiting for all clients to join...");
                // register a handler for the secondary connection internal event 
                this.eventEmitter.on('SECONDARY_CONNECTION', (num_clients) => {
                    console.log("Received a secondary connection event!");
                    // if all clients have joined 
                    if (num_clients === num_connections) {
                        console.log("All clients have joined. Sending ready events..");
                        const payload = (0, events_2.createEventPayload)(events_1.Events.READY_EVENT, {
                            data: {
                                ready: true,
                                room_id
                            }
                        });
                        // send ready events to all clients 
                        for (let connection_id of target_room.connections) {
                            this.connections[connection_id].send(payload);
                        }
                    }
                });
            }
            catch (err) {
                console.log(err.response.data);
            }
        });
        // to migrate a room to a particular server from this server 
        this.migrateInternal = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const { room_id } = req.params;
            const { to } = req.body;
            const target_room_index = this.rooms.findIndex(room => room.id === room_id);
            const room_clients = this.rooms[target_room_index].connections;
            const payload_1 = (0, events_2.createEventPayload)(events_1.Events.MIGRATION_EVENT, {
                data: {
                    room_id,
                    wait: true,
                }
            });
            // tell all the clients to stop any updates or sending any data to the server 
            for (let client of room_clients) {
                this.connections[client].send(payload_1);
            }
            console.log("Waiting for acknowledgement from all clients...");
            // register a handler for the MIGRATION_ACK internal event 
            this.eventEmitter.on('MIGRATION_ACK', (acknowledged) => {
                // if the migration event has been acknowledged by all clients 
                if (acknowledged) {
                    console.log("Received acknowledgements from all clients");
                    let room_data = this.rooms.find(room => room.id === room_id);
                    let state = {
                        id: room_data.id,
                        num_connections: room_data.connections.length
                    };
                    // respond to the other server's request with the state 
                    res.status(200).json(state);
                    const payload_2 = (0, events_2.createEventPayload)(events_1.Events.CONNECTION_REDIRECT_EVENT, {
                        data: {
                            url: to,
                            token: this.connection_token,
                            room_id
                        }
                    });
                    // send connection redirect events to all clients 
                    for (let connection_id of room_clients) {
                        this.connections[connection_id].send(payload_2);
                    }
                }
            });
        });
        this.app = (0, express_1.default)();
        this.connection_token = connection_token;
        this.primary_url = primary_url || null;
        this.scale_token = scale_token || null;
        this.max_connections = max_connections || 500;
        this.rooms = [];
        this.state = {};
        this.connections = {};
        this.eventEmitter = new events_3.default();
        this.onconnection = () => { };
        this.onclose = () => { };
        if (this.isPrimary()) {
            this.primaryState = {
                nodes: [
                    {
                        url: "",
                        rooms: [],
                        total_connections: 0,
                        lock: false
                    }
                ]
            };
        }
    }
    initMiddleware() {
        // mandatory middleware for express 
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
    }
    // initializes the routes for the rest api express server 
    initRoutes() {
        if (this.isPrimary()) {
            // get the current state of the primary server
            this.app.get('/state', this.getState);
            // list all the rooms on a specific node 
            this.app.post('/nodes', this.listRooms);
            // register secondary server with the primary
            this.app.post('/nodes/register', this.registerSecondary);
            // deregister secondary from primary 
            this.app.delete('/nodes/:server_url', this.deregisterSecondary);
            // to post state updates to primary server 
            this.app.post("/nodes/update", this.handleStateUpdates);
            // migrate a room from one node to another 
            this.app.post('/rooms/:room_id/migrate', this.migrateRoom);
            // to vacate a node (moves all connections and rooms out of the node to another node)
            this.app.get('/nodes/:server_url/vacate', this.vacateNode);
        }
        else {
            // to takeover a room from another secondary server 
            this.app.post('/internal/rooms/:room_id/takeover', this.takeoverRoom);
            // to migrate a room to another server 
            this.app.post('/internal/rooms/:room_id/migrate', this.migrateInternal);
        }
        // fallback route for requests to any invalid route
        this.app.all("*", (req, res) => res.status(404).json({ error: "route not found" }));
        // TODO add error handler here :- 
    }
    // to register event handlers 
    registerHandlers() {
        this.ws_server.on('connection', (ws, req) => {
            console.log("New connection received!");
            // listen for connection requests from clients  
            ws.on('message', (msg) => {
                // extract the event type and data from the message buffer 
                const { event, data } = (0, events_2.extractEventPayload)(msg);
                let data_obj;
                // check if this connection already exists on the server 
                if (this.connectionExists(ws) || (event && data)) {
                    switch (event) {
                        // sent by clients when connecting to the server for the first time 
                        case events_1.Events.CONNECTION_REQUEST_EVENT:
                            console.log("Received connection request event");
                            data_obj = JSON.parse(data);
                            this.handleConnectionRequest(ws, data_obj);
                            break;
                        // sent by clients when they have been redirected to connect to this server 
                        case events_1.Events.SECONDARY_CONNECTION_EVENT:
                            console.log("handling secondary connection!");
                            data_obj = JSON.parse(data);
                            this.handleConnection(ws, data_obj.data.room_id);
                            break;
                        // sent by clients to acknowledge migration of a room 
                        case events_1.Events.MIGRATION_ACK_EVENT:
                            console.log("Migration acknowledgement received from client");
                            data_obj = JSON.parse(data);
                            this.updateAckStatus(data_obj.data.room_id);
                            this.eventEmitter.emit('MIGRATION_ACK', this.getAckStatus(data_obj.data.room_id));
                            break;
                        default:
                            break;
                    }
                }
                // close the connection with a socket if it doesn't exist in the server's state or if invalid payload is sent 
                else {
                    ws.close();
                }
            });
            ws.on('error', (err) => {
                console.log("There was an error: ", err);
            });
            // if a client disconnects 
            ws.on("close", () => {
                console.log("A client has disconnected!");
                this.handleDisconnection(ws);
                this.onclose();
            });
        });
    }
    // handles first time connection requests
    handleConnectionRequest(ws, event) {
        const { data } = event;
        const { token, room_id } = data;
        let connection_url;
        // if a token has been sent and the token matches the server's connection token 
        if (token && token === this.connection_token) {
            console.log("Valid request establishing connection...");
            // if a room_id is specified find the node on which this exists
            if (room_id) {
                connection_url = this.findNodeWithRoom(room_id);
                // if no room with the specified id exists yet just find the most available node 
                if (!connection_url) {
                    connection_url = this.mostAvailableNode();
                }
            }
            else {
                connection_url = this.mostAvailableNode();
            }
            // if the most available node is the primary itself 
            if (connection_url === this.getRestHostname()) {
                console.log('Primary server is handling connection...');
                this.handleConnection(ws, room_id);
            }
            // tell the client to connect to the appropriate secondary server and close the connection
            else {
                console.log("Connection will be redirected to secondary server...");
                const payload = (0, events_2.createEventPayload)(events_1.Events.CONNECTION_REDIRECT_EVENT, {
                    data: {
                        url: connection_url,
                        token: this.connection_token,
                        room_id: room_id ? room_id : null
                    }
                });
                ws.send(payload);
            }
        }
    }
    // handles disconnection of clients which are maintained in state by the server 
    handleDisconnection(ws) {
        // check if this connection is present in the server's connection map
        if (this.connectionExists(ws)) {
            // target websocket index in the list of connections on the server 
            let connection_id;
            // index of the target websocket in the room 
            let ws_index;
            // index of the room to delte from the list of rooms on the server 
            let delete_room_index;
            // room id of the room from which the client has disconnected 
            let target_room_id;
            // get the connection id for the socket from the map of connections on this server 
            connection_id = this.getSocketConnectionId(ws);
            // remove the connection from any room 
            for (let i = 0; i < this.rooms.length; i++) {
                const room = this.rooms[i];
                ws_index = room.connections.indexOf(connection_id);
                if (ws_index !== -1) {
                    target_room_id = room.id;
                    this.rooms[i].connections.splice(ws_index, 1);
                    break;
                }
            }
            // delete a room if all clients have disconnected
            for (let i = 0; i < this.rooms.length; i++) {
                const room = this.rooms[i];
                if (room.connections.length === 0) {
                    delete_room_index = i;
                    break;
                }
            }
            // remove the room from the list of rooms on the server 
            if (delete_room_index !== undefined) {
                this.rooms.splice(delete_room_index, 1);
            }
            // remove the client from the list of clients on the server 
            if (connection_id) {
                delete this.connections[connection_id];
            }
            // update the state of the primary server 
            // if a connection was in a room 
            if (ws_index !== -1) {
                // if this is the primary server 
                if (this.isPrimary()) {
                    // get the index of the corrent node object in the primary state 
                    const target_node_index = this.primaryState.nodes.findIndex(node => node.url === this.getHostname());
                    // find the index of the target room (room from which a connection has been removed) on the target node
                    const room_index = this.primaryState.nodes[target_node_index].rooms.findIndex(room => room.id === target_room_id);
                    // update the total connections and no of connections in the room, on the target node 
                    this.primaryState.nodes[target_node_index].total_connections--;
                    this.primaryState.nodes[target_node_index].rooms[room_index].num_connections--;
                }
                // if this is not the primary server, send an api request to the primary to update state 
                else {
                    const api_endpoint = `${this.getPrimaryRestHost()}/nodes/update`;
                    (0, axios_1.default)(api_endpoint, {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        data: {
                            server_url: this.getHostname(),
                            rooms: this.rooms.map(room => ({ num_connections: room.connections.length, id: room.id })),
                            total_connections: this.getNumClients()
                        }
                    }).catch(err => console.log(err.response.data));
                }
            }
            // if there was no connection found in a room 
            else {
                if (this.isPrimary()) {
                    // find the target node and update primary state 
                    let target_node_index = this.primaryState.nodes.findIndex(node => node.url === this.getHostname());
                    if (target_node_index !== undefined) {
                        this.primaryState.nodes[target_node_index].total_connections--;
                    }
                }
                // send api request to primary to update state 
                else {
                    const api_endpoint = `${this.getPrimaryRestHost()}/nodes/update`;
                    (0, axios_1.default)(api_endpoint, {
                        method: "POST",
                        headers: { 'Content-Type': 'application/json' },
                        data: {
                            server_url: this.getHostname(),
                            rooms: this.rooms.map(room => ({ num_connections: room.connections.length, id: room.id })),
                            total_connections: this.getNumClients()
                        }
                    }).catch(err => console.log(err.response.data));
                }
            }
        }
    }
    // sets up the ws and rest api server and listens for connections on the specified port 
    listen(REST_PORT, host = "::", callback) {
        callback();
        this.port = REST_PORT.toString();
        this.api_server = http_1.default.createServer(this.app);
        this.ws_server = new WebSocket.Server({ server: this.api_server });
        // initialize middleware for express server
        this.initMiddleware();
        // initialize routes for expres server
        this.initRoutes();
        // registers event handlers for the websocket server 
        this.registerHandlers();
        // if this is the primary server set the url of the first node to the rest hostname  
        if (this.isPrimary()) {
            // since this is the primary and the first node will be the primary itself, set the server url equal to the hostname 
            this.primaryState.nodes[0].url = this.getHostname();
        }
        // listen for connections on the rest api server
        this.api_server.listen(parseInt(this.port) || 8001, host, () => {
            console.log(`Express server listening on port ${this.port}`);
        });
    }
    // returns true if the server has received acknowledgement from all its connected clients for a particular room
    getAckStatus(room_id) {
        const target_room_index = this.rooms.findIndex(room => room.id === room_id);
        return this.rooms[target_room_index].ack_count === this.rooms[target_room_index].connections.length;
    }
    // to update the number of acknowledgements received from clients during a migration
    updateAckStatus(room_id) {
        const target_room_index = this.rooms.findIndex(room => room.id === room_id);
        this.rooms[target_room_index].ack_count++;
    }
    // to update the migration status of a room 
    updateMigrationStatus(room_id, status) {
        const target_room_index = this.rooms.findIndex(room => room.id === room_id);
        this.rooms[target_room_index].migrating = status;
    }
    // checks if a given websocket connection exists in the server's state 
    connectionExists(ws) {
        if (Object.keys(this.connections).length === 0) {
            return false;
        }
        for (let socket of Object.values(this.connections)) {
            if (stringify(socket) === stringify(ws)) {
                return true;
            }
        }
        return false;
    }
    // finds the appropriate node for the given room_id and returns its url 
    findNodeWithRoom(room_id) {
        const nodes = this.primaryState.nodes;
        for (let node of nodes) {
            for (let room of node.rooms) {
                if (room.id === room_id) {
                    return node.url;
                }
            }
        }
        return null;
    }
    // returns the node with mininum no of connections
    mostAvailableNode() {
        const nodes = this.primaryState.nodes;
        let min_connection_node = nodes[0];
        if (nodes.length === 1) {
            return min_connection_node.url;
        }
        for (let node of nodes) {
            if (node.total_connections <= min_connection_node.total_connections) {
                min_connection_node = node;
            }
        }
        return min_connection_node.url;
    }
    // to register custom event handler 
    on(event, callback) {
        switch (event) {
            case 'connection':
                this.onconnection = callback;
                break;
            case 'close':
                this.onclose = callback;
                break;
            default:
                break;
        }
    }
    // to register middleware with the api server
    use(middleware) {
        this.app.use(middleware);
    }
    // checks if this is a primary server 
    isPrimary() {
        return !this.primary_url ? true : false;
    }
    // returns the connection id of the provided socket object in the connections map of the server 
    getSocketConnectionId(ws) {
        for (let key of Object.keys(this.connections)) {
            let socket = this.connections[key];
            if (stringify(socket) === stringify(ws)) {
                return key;
            }
        }
        return null;
    }
    // returns the number of connections on this server 
    getNumClients() {
        return Object.keys(this.connections).length;
    }
    // returns the host name (localhost:<PORT>)
    getHostname() {
        return `localhost:${this.port}`;
    }
    // returns thw websocket hostname (ws://<host>:<port>)
    getWsHostname() {
        return `ws://localhost:${this.port}`;
    }
    // returns the http hostname 
    getRestHostname() {
        return `http://localhost:${this.port}`;
    }
    // returns the http hostname of the primary server 
    getPrimaryRestHost() {
        return `http://${this.primary_url}`;
    }
    // to print server state to console 
    printState() {
        console.log("Primary state: ", this.primaryState);
        console.log("Total clients: ", this.getNumClients());
        console.log("Total rooms: ", this.rooms.length);
        console.log("ws hostname: ", this.getWsHostname());
    }
}
exports.default = Server;
