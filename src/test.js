"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Client_1 = __importDefault(require("./Client"));
const client = new Client_1.default('localhost:8002', 'abc');
(() => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield client.connect('yolo');
        console.log(response.message);
        setInterval(() => {
            let string = 'Just a random string';
            let buff = Buffer.from(string, 'utf-8');
            client.send(buff);
        }, 1000);
        client.on('message', (msg) => {
            console.log('Message received from server: ', msg.data);
        });
        client.on('close', () => {
            console.log("Server closed connection with client");
        });
        process.on('SIGINT', () => {
            client.socket.close();
        });
        process.on("SIGTERM", () => {
            client.socket.close();
        });
    }
    catch (err) {
        console.log(err);
    }
}))();
