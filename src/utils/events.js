"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.extractEventPayload = exports.createEventPayload = void 0;
function createEventPayload(event_type, event_data) {
    const data = JSON.stringify(event_data);
    const buffer = Buffer.alloc(data.length + 1);
    buffer[0] = event_type;
    buffer.write(data, 1);
    return buffer;
}
exports.createEventPayload = createEventPayload;
function extractEventPayload(buff) {
    const event_type = buff[0];
    const event_data = buff.slice(1).toString();
    return { event: event_type, data: event_data };
}
exports.extractEventPayload = extractEventPayload;
// const buff = createEventPayload(
//     Events.CONNECTION_REQUEST_EVENT, 
//     { 
//         data: { 
//             token: "abc",
//             message: "ok"
//         }
//     }
// );
// const { type, data } = extractEventPayload(buff);
// const data_obj = JSON.parse(data);
// console.log("Type: ", type);
// console.log("Data: ", data_obj.data);
