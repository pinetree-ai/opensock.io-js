import { BaseEvent, Events } from "../events";

export function createEventPayload(event_type: Events, event_data: BaseEvent){
    const data  = JSON.stringify(event_data);
    const buffer = Buffer.alloc(data.length + 1);

    buffer[0] = event_type;
    buffer.write(data, 1);

    return buffer;
}

export function extractEventPayload(buff: Buffer){
    const event_type = buff[0];
    const event_data = buff.slice(1).toString();

    return { event: event_type, data: event_data }; 
}

// const buff = createEventPayload(
//     Events.CONNECTION_REQUEST_EVENT, 
//     { 
//         data: { 
//             token: "abc",
//             message: "ok"
//         }
//     }
// );

// const { type, data } = extractEventPayload(buff);

// const data_obj = JSON.parse(data);

// console.log("Type: ", type);
// console.log("Data: ", data_obj.data);