// distributes n items into m groups and returns a mappinng such as:- {<group_name>: <item_list>, ...} 
export function distribute(rooms: Array<string>, servers: Array<string>){
    let mapping: any = {};
    let n = rooms.length;
    let m = servers.length;
    
    // add the server entries in the mapping object 
    for(let server of servers){
        mapping[server] = [];
    }   

    // if no of rooms is greater than no of servers 
    if(n > m){
        let num_partitions = Math.floor(n/m);
        let index = 0;

        for(let i=0; i < servers.length; i++){
            let key = servers[i];
            
            if(i === servers.length - 1){
                mapping[key] = rooms.slice(index, rooms.length);
            }
            else{
                mapping[key] = rooms.slice(index, index + num_partitions);
            }

            index += num_partitions;
        }
    } 
    // if no of rooms is lesser than no of servers || if no of rooms is equal to no of servers 
    else{
        for(let i =0; i< rooms.length; i++){
            let server = servers[i];
            mapping[server].push(rooms[i]);
        }
    }
   
    return mapping;
}

// let rooms = ['room_1', 'room_2', 'room_3', 'room_4'];
// let servers = ['localhost:8000','localhost:8001', 'locahost:8003'];

// console.log(distribute(rooms, servers));