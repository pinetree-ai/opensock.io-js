"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Server_1 = __importDefault(require("./Server"));
const server = new Server_1.default(null, 'abc', '123', 50);
server.on('connection', (socket, state, room) => {
    console.log('room: ', room);
    state.hello = 'world';
    console.log("State changed to : ", state);
    socket.on('message', (data) => {
        console.log("message received from client!");
        console.log('Data: ', data);
        console.log("Decoded 0-10: ", data.toString('utf8', 0, 10));
    });
});
server.listen(8002, '::', () => {
    console.log('started primary node!');
});
