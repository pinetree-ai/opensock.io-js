import WebSocket from 'isomorphic-ws';

interface SecondaryRoom{
    id: string;
    num_connections: number;
}

interface Node{
    url: string;
    rooms: Array<SecondaryRoom>,
    total_connections: number;
    lock: boolean;
}

interface PrimaryServerState {
    nodes: Array<Node>
}

interface Room {
    id: string;
    connections: Array<string>;
    migrating?: boolean; 
    ack_count?: number;
}

export { PrimaryServerState, Room };