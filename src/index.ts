import Server from './Server';
import { WebSocket } from 'ws';
import { PrimaryServerState, Room } from './types/server';

const server = new Server(null, 'abc', '123', 50);

server.on('connection', (socket: WebSocket, state: any, room: Room) => {
    console.log('room: ', room);
    state.hello = 'world';

    console.log("State changed to : ", state);

    socket.on('message', (data) => { 
        console.log("message received from client!");
        console.log('Data: ', data);
        console.log("Decoded 0-10: ", data.toString('utf8', 0, 10));
    });
});

server.listen(8002, '::', () => {
    console.log('started primary node!');
});