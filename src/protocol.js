const bitmasks = [1,2,4,8];
/*
    1 - 00000001
    2 - 00000010
    4 - 00000100
    8 - 00001000     
*/

const type = 1;
const data = JSON.stringify({ 
    token: "1234",
    message: "ok"
});
const buf = Buffer.alloc(data.length + 1);

buf[0] = type;
buf.write(data,1);

const is_true = (bitmasks[1] & buf[0]) === bitmasks[1];

console.log(buf.toString());
console.log(buf[0]);
console.log(is_true)

