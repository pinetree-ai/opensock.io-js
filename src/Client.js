"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const isomorphic_ws_1 = require("isomorphic-ws");
const events_1 = require("./events");
const events_2 = require("./utils/events");
class Client {
    constructor(primary_url, token) {
        this.primary_url = primary_url;
        this.token = token;
        this.wait = false;
        this.queue = [];
        this.onopen = () => { };
        this.onclose = () => { };
        this.onmessage = (data) => { };
        this.onerror = (error) => { };
    }
    connect(room_id) {
        return __awaiter(this, void 0, void 0, function* () {
            if (room_id) {
                this.room_id = room_id;
            }
            return new Promise((res, rej) => {
                this.socket = new isomorphic_ws_1.WebSocket('ws://' + this.primary_url);
                const payload = (0, events_2.createEventPayload)(events_1.Events.CONNECTION_REQUEST_EVENT, {
                    data: {
                        token: this.token,
                        room_id
                    }
                });
                this.socket.onopen = () => {
                    this.socket.send(payload);
                };
                this.socket.onclose = () => {
                    this.onclose();
                    return res({ message: "connection closed" });
                };
                this.socket.onerror = (err) => {
                    return rej(err);
                };
                this.socket.onmessage = (msg) => {
                    const { event, data } = (0, events_2.extractEventPayload)(msg.data);
                    let data_obj, event_payload;
                    switch (event) {
                        case events_1.Events.CONNECTION_SUCCESS_EVENT:
                            console.log("Connection to server established!");
                            data_obj = JSON.parse(data);
                            return res(data_obj.data);
                        case events_1.Events.CONNECTION_REDIRECT_EVENT:
                            console.log("Auto reconnecting to secondary server...");
                            data_obj = JSON.parse(data);
                            this.handleReconnection(data_obj.data, res, rej);
                            break;
                        case events_1.Events.MIGRATION_EVENT:
                            // console.log("Received migratin event");
                            // send acknowledgement of event here 
                            data_obj = JSON.parse(data);
                            event_payload = (0, events_2.createEventPayload)(events_1.Events.MIGRATION_ACK_EVENT, {
                                data: {
                                    room_id: data_obj.data.room_id,
                                    ack: true
                                }
                            });
                            this.socket.send(event_payload);
                            console.log("Sent migration ack event");
                            this.wait = true;
                            break;
                        case events_1.Events.READY_EVENT:
                            this.wait = false;
                            this.send();
                            break;
                        default:
                            this.onmessage(msg);
                            break;
                    }
                };
            });
        });
    }
    handleReconnection(data, res, rej) {
        // close the old connection
        this.socket.close();
        console.log("Closed old connection");
        const { url, token, room_id } = data;
        if (room_id) {
            this.room_id = room_id;
        }
        this.socket = new isomorphic_ws_1.WebSocket('ws://' + url);
        console.log("created new connection");
        const payload = (0, events_2.createEventPayload)(events_1.Events.SECONDARY_CONNECTION_EVENT, {
            data: { token, room_id }
        });
        this.socket.onopen = () => {
            console.log("Sending secondary connection event");
            this.socket.send(payload);
            console.log("sent secondary connection event");
        };
        console.log("Added on open handler");
        this.socket.onmessage = (msg) => {
            const { event, data } = (0, events_2.extractEventPayload)(msg.data);
            let data_obj, event_payload;
            switch (event) {
                case events_1.Events.CONNECTION_SUCCESS_EVENT:
                    data_obj = JSON.parse(data);
                    if (res) {
                        return res(data_obj.data);
                    }
                    else {
                        return;
                    }
                case events_1.Events.CONNECTION_REDIRECT_EVENT:
                    console.log("Auto reconnecting to secondary server...");
                    data_obj = JSON.parse(data);
                    console.log("data obj: ", data_obj);
                    this.handleReconnection(data_obj.data, res, rej);
                    break;
                case events_1.Events.MIGRATION_EVENT:
                    console.log("Received migration event");
                    data_obj = JSON.parse(data);
                    event_payload = (0, events_2.createEventPayload)(events_1.Events.MIGRATION_ACK_EVENT, {
                        data: {
                            room_id: data_obj.data.room_id,
                            ack: true
                        }
                    });
                    this.socket.send(event_payload);
                    this.wait = true;
                    break;
                case events_1.Events.READY_EVENT:
                    this.wait = false;
                    this.send();
                    break;
                default:
                    this.onmessage(msg);
                    break;
            }
        };
        console.log("Added on message handler");
        this.socket.onclose = () => {
            this.onclose();
            if (res) {
                return res({ message: "connection closed" });
            }
            else {
                return;
            }
        };
        console.log("Added on close handler");
        this.socket.onerror = (err) => {
            this.onerror(err);
            if (rej) {
                return rej(err);
            }
            else {
                return;
            }
        };
        console.log("Added on error handler");
    }
    send(data) {
        if (this.wait && data) {
            this.queue.push(data);
        }
        else if (!this.wait && this.queue.length > 0) {
            while (this.queue.length !== 0) {
                const data = this.queue.shift();
                this.socket.send(data);
            }
        }
        else {
            if (data) {
                this.socket.send(data);
            }
        }
    }
    on(event, callback) {
        switch (event) {
            case 'close':
                this.onclose = callback;
                break;
            case 'message':
                this.onmessage = callback;
                break;
            default:
                break;
        }
    }
}
exports.default = Client;
