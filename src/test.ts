import Client from './Client';

const client = new Client('localhost:8002', 'abc');

(async () => {
    try{
        const response: any = await client.connect('yolo');
        console.log(response.message);

        setInterval(() => {
            let string = 'Just a random string';
            let buff = Buffer.from(string, 'utf-8');

            client.send(buff)
        }, 1000)

        client.on('message', (msg: any) => { 
            console.log('Message received from server: ', msg.data);
        });
        
        client.on('close', () => { 
            console.log("Server closed connection with client");
        });

        process.on('SIGINT', () => { 
            client.socket.close();
        });

        process.on("SIGTERM", () => { 
            client.socket.close();
        });
    }
    catch(err){
        console.log(err);
    }
})();