import { BaseEvent } from './events';

export interface ConnectionSuccessEvent extends BaseEvent{
    data: {
        message: string;
    }
}