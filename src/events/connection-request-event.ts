import { BaseEvent } from './events';

export interface ConnectionRequestEvent extends BaseEvent{
    data: {
        token: string;
        room_id?: string;
    }
}
