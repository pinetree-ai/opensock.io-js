import { BaseEvent } from './events';

export interface MigrationEvent extends BaseEvent{
    data: {
        room_id: string;
        wait: boolean;
    }
};