export interface BaseEvent{ 
    data: any,
}

export enum Events{ 
    CONNECTION_REQUEST_EVENT = 1,               // Received by only a primary server, when a client wants to connect to server 
    CONNECTION_SUCCESS_EVENT = 2,               // Published by a server when a successful connection is established with a client
    CONNECTION_REDIRECT_EVENT = 3,              // when the primary tells the client to connect to a secondary server 
    SECONDARY_CONNECTION_EVENT = 4,             // When a client wants to connect to a secondary server (should be sent to and handled by only a secondary server)
    MIGRATION_EVENT = 5,                        // send from a sever hosting a room to all its clients when a migration is about to happen
    MIGRATION_ACK_EVENT = 6,                    // sent by a client to a server acknowledging the migration wait state 
    READY_EVENT = 7                             // sent by a server to its clients when migration is complete telling them that they can start sending data again 
}
