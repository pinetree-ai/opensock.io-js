import { BaseEvent } from './events';

export interface ConnectionRedirectEvent extends BaseEvent{ 
    data: { 
        url: string;
        token: string;
        room_id?: string | null;
    }
}