import { BaseEvent } from './events';

export interface MigrationAckEvent extends BaseEvent{
    data: {
        room_id: string;
        ack: boolean;
    }
};