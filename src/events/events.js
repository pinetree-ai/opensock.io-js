"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Events = void 0;
var Events;
(function (Events) {
    Events[Events["CONNECTION_REQUEST_EVENT"] = 1] = "CONNECTION_REQUEST_EVENT";
    Events[Events["CONNECTION_SUCCESS_EVENT"] = 2] = "CONNECTION_SUCCESS_EVENT";
    Events[Events["CONNECTION_REDIRECT_EVENT"] = 3] = "CONNECTION_REDIRECT_EVENT";
    Events[Events["SECONDARY_CONNECTION_EVENT"] = 4] = "SECONDARY_CONNECTION_EVENT";
    Events[Events["MIGRATION_EVENT"] = 5] = "MIGRATION_EVENT";
    Events[Events["MIGRATION_ACK_EVENT"] = 6] = "MIGRATION_ACK_EVENT";
    Events[Events["READY_EVENT"] = 7] = "READY_EVENT"; // sent by a server to its clients when migration is complete telling them that they can start sending data again 
})(Events = exports.Events || (exports.Events = {}));
