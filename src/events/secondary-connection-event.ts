import {  BaseEvent } from './events';

export interface SecondaryConnectionEvent extends BaseEvent{
    data: {
        token: string;
        room_id?: string;
    }
}