import { BaseEvent } from './events';

export interface ReadyEvent extends BaseEvent{
    data: {
        room_id: string;
        ready: boolean;
    }
}