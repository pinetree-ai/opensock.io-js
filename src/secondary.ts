import Server from './Server';
import { WebSocket } from 'ws';
import { PrimaryServerState, Room } from './types/server';

const args = process.argv.slice(2);
const port = args[1];

const server = new Server('localhost:8002', 'abc', '123', 50);

server.on('connection', (socket: WebSocket, state: PrimaryServerState, room: Room) => {
    console.log('room: ', room);

    socket.on('message', (data) => { 
        console.log("message received from client: ", data.toString());
    });
});

// Listen on localhost:8002 for websocket connections
server.listen(parseInt(port) || 8004, "::", () => {
    console.log('Started secondary node');
});