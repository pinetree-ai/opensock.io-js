"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Server_1 = __importDefault(require("./Server"));
const args = process.argv.slice(2);
const port = args[1];
const server = new Server_1.default('localhost:8002', 'abc', '123', 50);
server.on('connection', (socket, state, room) => {
    console.log('room: ', room);
    socket.on('message', (data) => {
        console.log("message received from client: ", data.toString());
    });
});
// Listen on localhost:8002 for websocket connections
server.listen(parseInt(port) || 8004, "::", () => {
    console.log('Started secondary node');
});
