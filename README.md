# Opensock.io

This project is created and maintained by [Inventives, Inc.](https://inventives.ai), and is licensed under the [Creative Commons Attribution-NonCommercial 4.0 International License](https://creativecommons.org/licenses/by-nc/4.0/legalcode).

**Opensock** is a scalable secure WebSocket solution, built as a wrapper around traditional WebSockets but with services to support scalability. The issue with conventional WebSockets is that they cannot be easily scaled down as they need to maintain an open connection between the client and server.

**Opensock** solves this issue by supporting multiple WebSocket server nodes on dedicated domains/subdomains as you wish, and a primary server node orchestrating connections with the client. The primary server node maintains a high-performance in-memory map of active server nodes and connection details. In order to scale down, migration requests may be made to the primary node which handle state migration from the source server to destination server. Upon successful migration, the connection clients are informed of the migration and automatically reconnect to the new server nodes.

There are two components in this package: the *Client* and *Server*, and opensock handles authentication, reconnection, and migration.

You still need a scaling intelligence solution that decides which nodes need to be migrated, scaled, down, or added.

## Installation

```
npm install opensock.io
```

## Usage

In order to handle migrations, you need to clearly define the "state" on the server. The state can handle any information, including members in a chat room, previous context, stored data, etc., however the state has to be an object in which any state variables must be properties of the state object.

### Primary Server Node

```js
import { Server } from 'opensock.io';

const server = new Server(null, CONNECTION_TOKEN, SCALE_TOKEN);

// The Express.JS server is used in the backend along with WebSockets, you can pass any middleware such as CORS policy, etc.
import cors from 'cors';
server.use(cors({ origin: "*.mydomain.com" }));

// Handle connections
server.on('connection', (socket, state, room) => {

    // Add any state variables?
    state.hello = 'world';

    socket.on('message', data => {

        // Handle message here...

        // Example - broadcast message to other clients in the room
        room.clients.forEach(client => client.send(data));

        // Example - send a message back
        socket.send({ foo: 'bar' });
    });
});

// Handle connection close
server.on('close', () => {

    // Do something here...

});

// Listen on localhost:8001 for websocket connections and localhost:8000 for scale requests
server.listen('localhost:8001', 'localhost:8000', () => {
    console.log('Started primary opensock node');
});
```

### Secondary Server Node

```js
import { Server } from 'opensock.io';

const server = new Server(PRIMARY_NODE_URL, CONNECTION_TOKEN);
// Connection token must match primary node connection token

// Have connections, messages, and close the same way...

// Listen on localhost:8002 for websocket connections
server.listen('localhost:8002', null, () => {
    console.log('Started opensock node');
});
```

### Client

```js
import { Client } from 'opensock.io';

const client = new Client(PRIMARY_URL, CONNECTION_TOKEN);

// Connect to a server as an individual connection
await client.connect();

// Connect to a specific room with a unique ID (can be any arbitrary string)
await client.connect(ROOM_ID);

client.on('message', data => {
    // Handle message here...
});
```

### Scaling & Migrating

Make an API calls to the primary node scale server (in this example: `http://localhost:8000`) with the header:

```json
{
    "Authorization": "Bearer {SCALE_TOKEN}"
}
```

If authorization fails, you will receive a **401 Not Authorized** status.

#### List rooms on server node

- **GET** `/nodes/localhost:8002`
- Returns status: **200 OK** with JSON list of room Ids

#### Register secondary server node with primary

- **POST** `/nodes`
- Body: `{ "url": "localhost:8002" }`
- Returns status: **200 OK**

#### Deregister secondary server node

- **DEL** `/nodes/localhost:8002`
- Return status: **200 OK** if successful or,
- **406 Not Acceptable** if server hasn't been cleared yet.

#### Migrate room from one node to another (secondary to primary)

- **POST** `/rooms/{roomId}/migrate`
- Body: `{ "url": "localhost:8002" }`
- Returns status: **200 OK** if state migration was successful or,
- **202 Accepted** if state migration was started.

#### Track room migration status

- **GET** `/rooms/{roomId}`
- Returns status: **200 OK** with status message

#### Vacate a node server

Running this automatically migrates all rooms from the provided node to other avaialble nodes.

- **POST** `/nodes/localhost:8002/vacate`
- Returns status: **200 OK** if state migration was successful or,
- **202 Accepted** if state migration was started but is still in progress, or
- **406 Not Acceptable** if there is not enough capacity on remaining servers (capacities must be set and updated manually to use intelligent allocation).