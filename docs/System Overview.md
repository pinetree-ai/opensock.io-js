# System Overview

Opensock.io is a WebSocket wrapper which allows sockets to be scaled automatically. This is done by facilitation of socket room migrations between servers with minimal delay and no additional work on the client side.

A deployed Opensock.io system includes a primary server, optionally multiple secondary servers, and any clients that wish to connect to the servers. Each server, whether primary or secondary is called a **node**.

Clients connect to the primary server which performs authorization and load balancing between the nodes and redirect the client to the selected node which hosts the room the client is attempting to connect to or to the most available node.

The system implements rooms to keep state for connections together. For instance, if multiple clients need to be on the same socket server because of forwarding, all these clients must be in the same "room".