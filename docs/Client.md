# Client

An Opensock.io client is simply a WebSocket client wrapped with some protocol header augmentation and functionality to handle server redirection and reconnection.

In order to use the Opensock.io client using NPM, you must be able to install it via:

```
npm install opensock.io
```

You can import the client from the library as follows:

```js
import { Client } from 'opensock.io';
```

## Class Constructor

You must be able to instantiate an Opensock.io client as follows:

```js
const client = new Client(PRIMARY_URL, CONNECTION_TOKEN);
```

- `PRIMARY_URL` is the URL to the primary server node. For example, if you've deployed your servers in `*.myapp.com` with the primary server located at `srv0.myapp.com` (and optionally, other secondary servers at `srv1.myapp.com`, `srv2.myapp.com` etc.), the PRIMARY_URL would be `srv1.myapp.com`.

- `CONNECTION_TOKEN` is a unique secure string you can pass in to only allow authorized clients to connect to the server. You can configure the connection token on the primary server.

### Under the hood

- Save primary URL and connection token for use later

## Connect to Server

In order to connect to a socket server and communicate with a room, you can do:

```js
await client.connect(ROOM_ID);
```

- `ROOM_ID` is unique string identifier for a room.

If you don't have a room to connect to, you can leave the argument empty and the function returns an automatically generated room ID if you need to share it with anyone to connect to it.

```js
const roomId = await client.connect();
```

The client connect function is asynchronous and should always be awaited. This function should only resolved after the connection to the destination server has been opened.

### Under the hood

- Connect to the primary server URL with the connection token and provide the room ID (if exists - if not, generate a random UUID to use)
- If the primary server wishes to redirect the client to a secondary server that's managing the room or has better availability, it will return with a redirect header which contains the secondary server URL
- Connect to the secondary server URL with the connection token and room ID
- Return a promise that gets resolved when the WebSocket client's `connection.onopen` is triggered.

## Stream data to the Server

You can send data to the server simply by running:

```js
client.send(DATA);
```

## Receive data from the Server

Set up the data receipt handler using:

```js
client.on('message', data => {
    // Handle message here...
});
```

When data is received by the client, it should call the callback function passed in by the user/developer above.

To handle socket closure, you can set a handler callback as follows:

```js
client.on('close', () => {
    // Handle close here...
})
```

### Under the hood

- Implement WebSocket on message handler.
- Check for headers and handle.
- Call the `client.on('message', callback)` callback function with the packet data as the input argument.

### Migrating Header

If the client receives a migration header, it means the server room is currently being migrated and the state should NOT be changed.

Instead of sending any more client data to the server, it should keep it in memory until it receives a reconnect header from the server.

### Reconnect Header

A reconnect header is received if a server room was just migrated to another server. The original server connection must be closed and a new connection must be opened to the provided server URL in the message.

There may be packets in the local state which were never sent to the server because the migration was in process. These packets must be sent to the newly connected server.