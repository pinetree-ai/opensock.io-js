# Server

An Opensock.io server is a WebSocket server wrapped with additional functionality for room management, state migration support, authorization, and load balancing.

In an Opensock.io server cluster, there must be one server configured as the primary and the others are considered secondary servers.

In order to implement an Opensock.io server, you can install the Opensock.io library via NPM:

```
npm install opensock.io
```

You can import the server from the library as follows:

```js
import { Server } from 'opensock.io';
```

## Class Constructor

To instantiate a server as the primary server node, pass `null` as the primary server URL, and additionally pass in the scale token to be used to authorize API requests to perform migrations or vacate a server.

```js
const server = new Server(null, CONNECTION_TOKEN, SCALE_TOKEN);
```

- `SCALE_TOKEN` is a secure string which should be passed in HTTP(S) requests to the primary server in order to perform scaling operations.

To instantiate a server as a secondary server node, pass the API URL for the primary server API as the first argument.

```js
const server = new Server(PRIMARY_URL, CONNECTION_TOKEN);
```

# Under the hood

- Save the connection token to authorize socket requests with.
- Save the scale token to authorize API scaling requests with.

## Server Middleware

An Express.JS server is used in the backend along with a WebSocket server. To provide any middleware such as CORS, etc., you can use the `use` function.

For example, to provide a CORS policy:

```js
import cors from 'cors';
server.use(cors({ origin: "*.mydomain.com" }));
```

## Connection Handler

To handle connections, use:

```js
server.on('connection', (socket, state, room) => {
    // Handle connection here
});
```

- The `socket` argument to the callback is the WebSocket socket.
- The `state` argument is a reference to an object in which the room's state can be stored.
- The `room` argument includes the room Id and a list of members connected to the room.

To handle messages from the socket in a connection, you can do:

```js
socket.on('message', data => {
    // Handle message here...
});
```

To send messages back to the client in a connection, you can do:

```js
socket.send(DATA);
```

### Under the hood

When a new connection is made to a primary server for an existing room that's not on the primary server, it returns a redirect header with the URL to the secondary server handling the room.

When a connection is made to a primary server for a new room, it determines the best server to host the room between itself and any registered secondary servers. If it determines the best server is not itself, it returns a redirect header with the URL to the secondary server for the room. It also saves to local state that the specific secondary server is hosting the room with the given Id.

If the primary server determines that the connection should be managed by itself, then the connection handler callback is called with the arguments.

If a secondary server receives a redirected connection from a client, it registers the room state if not already (or if it exists, adds the new connection client as a member to the room), and calls the connection handler callback with the arguments.

## Start Server

All Opensock.io servers listen for API requests and Websocket requests. To start the server and listen for HTTP(S) and WebSocket connections, run:

```js
server.listen(API_HOST, WS_HOST, () => {
    // Server is ready...
});
```

- `API_URI` is the hostname and port for the API server to handle inter-node communications for scaling, migration, etc. and for external scaling requests.

- `WS_URI` is the hostname and port to listen for WebSocket connections.

### Under the hood

- Launch the API server.
- Launch the WebSocket server.

## API Server

### External Routes

The API server for scaling must host the following endpoints:

#### List rooms on server node

- **GET** `/nodes/localhost:8002`
- Returns status: **200 OK** with JSON list of room Ids

#### Register secondary server node with primary

- **POST** `/nodes`
- Body: `{ "url": "localhost:8002" }`
- Returns status: **200 OK**

#### Deregister secondary server node

- **DEL** `/nodes/localhost:8002`
- Return status: **200 OK** if successful or,
- **406 Not Acceptable** if server hasn't been cleared yet.

#### Migrate room from one node to another (secondary to primary)

- **POST** `/rooms/{roomId}/migrate`
- Body: `{ "url": "localhost:8002" }`
- Returns status: **200 OK** if state migration was successful or,
- **202 Accepted** if state migration was started.

#### Track room migration status

- **GET** `/rooms/{roomId}`
- Returns status: **200 OK** with status message

#### Vacate a node server

Running this automatically migrates all rooms from the provided node to other avaialble nodes.

- **POST** `/nodes/localhost:8002/vacate`
- Returns status: **200 OK** if state migration was successful or,
- **202 Accepted** if state migration was started but is still in progress, or
- **406 Not Acceptable** if there is not enough capacity on remaining servers (capacities must be set and updated manually to use intelligent allocation).

### Internal Routes

Additionally, any internal routes to be used for scaling must be under the `/internal/*` subroute.

#### Takeover a room from a server

This is used when one node (primary or secondary) should be the new room host. The new host makes the following request:

- **POST** `/internal/rooms/{roomId}/migrate`
- Body: `{ "url": "localhost:8002" }` (of self)

When this request is received, the server receiving it should send a migration header to all connected clients letting them know to hold off on sending anything that would change the state.

It should then return with the state for the room and then send a redirect header to all clients in the room with the server URL of the request sender.